require 'rails_helper'
RSpec.describe ApplicationHelper, type: :helper do
  describe '#full_title' do
    it { expect(full_title("")).to eq Const::BASE_TITLE }
    it { expect(full_title(nil)).to eq Const::BASE_TITLE }
    it { expect(full_title("sample")).to eq "sample - #{Const::BASE_TITLE}" }
  end
end
