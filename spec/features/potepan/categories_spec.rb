require 'rails_helper'

RSpec.feature "Potepan::Categories", type: :feature do
  let!(:taxonomy) { create(:taxonomy) }
  let!(:taxon) { create(:taxon, parent_id: taxonomy.root.id) }
  let!(:product) { create(:product, taxons: [taxon]) }

  before do
    visit potepan_category_path(taxon.id)
  end

  it "taxonカテゴリの商品名と値段が表示、商品名をクリックすると商品ページへ遷移" do
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
    click_on product.name
    expect(current_path).to eq potepan_product_path(product.id)
  end

  it "カテゴリリストが表示、taxon名をクリックするとカテゴリページへ遷移" do
    expect(page).to have_content taxonomy.name
    expect(page).to have_content taxon.name
    click_on taxon.name
    expect(current_path).to eq potepan_category_path(taxon.id)
  end
end
