require 'rails_helper'

RSpec.feature "Potepan::Products", type: :feature do
  let(:taxonomy)           { create(:taxonomy) }
  let(:taxon)              { create(:taxon, taxonomy: taxonomy) }
  let(:taxon2)             { create(:taxon, taxonomy: taxonomy) }
  let(:product)            { create(:product, taxons: [taxon]) }
  let(:notaxon_product)    { create(:product) }
  let!(:related_products)  { create_list(:product, 5, taxons: [taxon]) }
  let!(:related_products2) { create(:product, taxons: [taxon2]) }

  describe "商品詳細ページのテンプレート" do
    before do
      visit potepan_product_path(product.id)
    end

    it "一覧ページへ戻るリンクが正常に動作する" do
      expect(page).to have_link "一覧ページへ戻る"
      click_link "一覧ページへ戻る"
      expect(current_path).to eq potepan_category_path(taxon.id)
    end

    it "taxonに紐付かない商品の場合、'一覧ページへ戻る'は表示されない" do
      visit potepan_product_path(notaxon_product.id)
      expect(page).not_to have_content "一覧ページへ戻る"
    end

    it "関連商品をクリックすると、クリックした関連商品ページに遷移する" do
      click_on related_products[0].name
      expect(current_path).to eq potepan_product_path(related_products[0].id)
    end

    it "関連商品にメインの商品は含まれない" do
      expect(product.related_products).not_to include product
    end

    it "関連しない商品は表示されない" do
      expect(page).not_to have_content related_products2
    end

    it "関連商品を5件作成し、4件のみ表示される" do
      expect(page).to have_selector ".productBox", count: 4
    end
  end
end
