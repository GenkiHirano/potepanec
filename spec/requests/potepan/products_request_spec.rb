require 'rails_helper'

RSpec.describe "Potepan::Products", type: :request do
  describe "GET #index" do
    before do
      get potepan_index_path
    end

    it "indexページのhttpリクエストは成功する" do
      expect(response).to have_http_status(200)
    end
  end

  describe "GET #show" do
    let(:master_variant)   { create(:master_variant, images: [create(:image)]) }
    let(:taxonomy)         { create(:taxonomy) }
    let(:taxon)            { create(:taxon, taxonomy: taxonomy) }
    let(:product)          { create(:product, master: master_variant, taxons: [taxon]) }
    let!(:related_product) { create(:product, taxons: [taxon]) }

    before do
      get potepan_product_path(product.id)
    end

    it "productsページのhttpリクエストは成功する" do
      expect(response).to have_http_status(200)
    end

    it "商品名が正しく含まれる" do
      expect(response.body).to include product.name
    end

    it "商品の説明が正しく含まれる" do
      expect(response.body).to include product.description
    end

    it "商品の値段が正しく含まれる" do
      expect(response.body).to include product.display_price.to_s
    end

    it "商品の画像は正しく含まれる" do
      for image in product.images
        expect(response.body).to include image.attachment(:large)
        expect(response.body).to include image.attachment(:small)
      end
    end

    it "関連商品名が正しく含まれる" do
      expect(response.body).to include related_product.name
    end

    it "関連商品の値段が正しく含まれる" do
      expect(response.body).to include related_product.display_price.to_s
    end
  end
end
