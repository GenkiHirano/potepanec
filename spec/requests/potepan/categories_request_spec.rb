require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  describe "GET #show" do
    let(:taxon) { create(:taxon) }
    let(:taxonomy) { create(:taxonomy) }
    let(:product) { create(:product) }

    before do
      get potepan_category_path(taxon.id)
    end

    it "showページのhttpリクエストは成功する" do
      expect(response).to have_http_status(200)
    end

    it "分類群(taxon)が表示される" do
      expect(response.body).to include taxon.name
    end

    it "分類学(taxonmies)が表示される" do
      expect(response.body).to include taxonomy.name
    end
  end
end
